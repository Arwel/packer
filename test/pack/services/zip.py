from multiprocessing.pool import Pool
from pathlib import Path
from typing import Iterable
from zipfile import ZipFile


class Zip:
    def __init__(self, dest_path):
        self._dest_path = dest_path

    def _walk(self, path: Path) -> None:
        with ZipFile(str(self._dest_path) + '/' + str(path.name) + '.zip', 'w') as zip_file:
            self.zip(zip_file, path)

    def zip(self, zip_file: ZipFile, path: Path) -> None:
        if path.is_dir():
            for item in path.iterdir():
                self.zip(zip_file, item)
        else:
            arc_name = str(path).replace(str((Path('.')).cwd()) + '/', '')
            print(arc_name)
            zip_file.write(str(path), arc_name)

    def run(self, path_list: Iterable) -> None:
        pool = Pool(10)
        result = pool.map_async(self._walk, path_list)
        result.wait()
