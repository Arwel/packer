#! /usr/bin/python
# from pathlib import Path

# from services.copy import Copy
# from services.delete import Delete
# from services.index import Index
# from services.zip import Zip

from config.configurator import Configurator
from app.app import App
from pathlib import Path
# def copy_main():
#     current_path = str((Path('.')).cwd())
#     copy = Copy(Path(current_path + '/test/'))
#     copy.run([Path(current_path)])


# def zip_main():
#     current_path = str((Path('.')).cwd())
#     zipper = Zip(Path(current_path + '/test/'))
#     zipper.run([Path(current_path)])


# def delete_main():
#     current_path = str((Path('.')).cwd())
#     cleaner = Delete()
#     cleaner.run([Path(current_path + '/test/')])


# def index_main():
#     current_path = str((Path('.')).cwd())
#     indexer = Index()
#     indexer.run([Path(current_path)])
#     print(Index.indexed_list)


def main():
    configurator = Configurator()
    configurator.load(str((Path('.')).cwd()) + '/.pack.json')
    app = App(configurator)
    app.run()


if __name__ == '__main__':
    main()
