import argparse
import json
from typing import Dict
from pathlib import Path


class Configurator:
    _inst = None
    _config: Dict = {}
    _args = None

    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-I', '--install',
                            help='Install packer', nargs='?', default=1)
        parser.add_argument('-i', '--index', help='Index',
                            nargs='?', default=1)
        parser.add_argument('-z', '--zip', help='Zip',
                            nargs='?', default=1)
        parser.add_argument('-r', '--remove', help='Remove',
                            nargs='?', default=1)
        parser.add_argument('-o', '--option', help='Option',
                            nargs='?', default=1)
        parser.add_argument('-k', '--key', help='Option key',
                            nargs='?', default=1)
        parser.add_argument('-v', '--value', help='Option value',
                            nargs='?', default=1)
        parser.add_argument('-c', '--copy', help='Copy',
                            nargs='?', default=1)
        parser.add_argument(
            '-t', '--test', help='Test', nargs='?', default=1)
        Configurator._args = parser.parse_args()

    def get_flag(self, key):
        return self.get_param(key) != 1

    def get_param(self, key):
        return getattr(self._args, key)

    def get(self, key, default=None):
        return self._config[key] if key in self._config else default

    @staticmethod
    def inst():
        if Configurator._inst is None:
            Configurator._inst = Configurator()

        return Configurator._inst

    def load(self, path: str):
        _path = Path(path)
        if _path.exists():
            with open(path) as config_file:
                try:
                    self._config = json.loads(config_file.read())
                except IOError:
                    print('IO Exception')
                    self._config = {}
        else:
            print('No config found init with default values')
            self._config = {}

    def save(self, path):
        with open(path, 'w') as config_file:
            try:
                config_file.write(json.dumps(self._config))
            except IOError:
                print('IO Exception')
                self._config = {}

    def get_config(self):
        return self._config

    def update_config(self, key, value):
        self._config[key] = value
