from actions.installaction import InstallAction
from actions.installaction import Action
from config.configurator import Configurator
from actions.indexaction import IndexAction
from actions.cleanaction import CleanAction
from actions.optionaction import OptionAction
from actions.copyaction import CopyAction
from actions.zipaction import ZipAction


class App:
    def __init__(self, configurator: Configurator):
        self._configurator = configurator

    def run(self):
        action = Action(self._configurator)
        if self._configurator.get_flag('install'):
            action = InstallAction(self._configurator)
        elif self._configurator.get_flag('test'):
            print('test')
        elif self._configurator.get_flag('index'):
            action = IndexAction(self._configurator)
        elif self._configurator.get_flag('remove'):
            action = CleanAction(self._configurator)
        elif self._configurator.get_flag('option'):
            action = OptionAction(self._configurator)
        elif self._configurator.get_flag('copy'):
            action = CopyAction(self._configurator)
        elif self._configurator.get_flag('zip'):
            action = ZipAction(self._configurator)

        action.run()
