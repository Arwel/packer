from actions.action import Action
from services.delete import Delete
from pathlib import Path


class CleanAction(Action):
    def run(self):
        path_list = self._configurator.get('path_list')
        if path_list is None:
            path_list = (Path('.')).cwd().iterdir()
        else:
            path_list = map(lambda item: Path(item), path_list)

        cleaner = Delete()
        cleaner.run(path_list)
