from actions.action import Action
from pathlib import Path
from services.zip import Zip


class ZipAction(Action):
    def run(self):
        dest = self._configurator.get('dest')
        dest_path = Path(dest)

        if dest is None or not dest_path.exists():
            print('Wrong dest path')
            return None

        path_list = self._configurator.get('path_list')
        if path_list is None:
            path_list = (Path('.')).cwd().iterdir()
        else:
            path_list = map(lambda item: Path(item), path_list)

        zipper = Zip(dest_path)
        zipper.run(path_list)
