from pathlib import Path

import shutil

import os

from actions.action import Action
from services.copy import Copy


class InstallAction(Action):
    def run(self):
        bin_path = '/usr/local/bin'
        src = (Path()).cwd()
        src_name = '/' + src.name

        symlink_path = Path(bin_path + '/packer')
        if symlink_path.exists():
            os.remove(bin_path + '/packer')

        if (Path(bin_path + src_name)).exists():
            shutil.rmtree(bin_path + src_name, True)
        copy_service = Copy(bin_path)
        copy_service.run((src,))
        os.system('chmod a+x ' + bin_path + src_name + '/pack.py')

        os.symlink(bin_path + src_name + '/pack.py', bin_path + '/packer')
