from actions.action import Action
from pathlib import Path


class OptionAction(Action):
    def run(self):
        key = self._configurator.get_param('key')
        value = self._configurator.get_param('value')

        if len(key) < 1:
            print('Can not set empty key')
            return None

        current_path = (Path('.')).cwd()
        config_path = str(current_path) + '/.pack.json'
        self._configurator.update_config(key, value)
        self._configurator.save(config_path)
