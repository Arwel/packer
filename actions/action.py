from config.configurator import Configurator


class Action:
    def __init__(self, configurator: Configurator):
        self._configurator = configurator

    def run(self):
        pass
