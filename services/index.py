from multiprocessing.pool import ThreadPool
from pathlib import Path
from typing import List


class Index:
    indexed_list: List = []

    def _walk(self, path: Path):
        if path.is_dir():
            for item in path.iterdir():
                self._walk(item)
        elif path.name == '.pack':
            Index.indexed_list.append(str(path.parent))

    def run(self, path_list):
        pool = ThreadPool(10)
        result = pool.map_async(self._walk, path_list)
        result.get()

    def get_list(self):
        return self.indexed_list
