from multiprocessing.pool import Pool

from shutil import copytree


class Copy:
    def __init__(self, dest_path):
        self._dest_path = dest_path

    def _walk(self, path):
        copytree(str(path), str(self._dest_path) + '/' + str(path.name), symlinks=True)

    def run(self, path_list):
        pool = Pool(10)
        result = pool.map_async(self._walk, path_list)
        result.wait()
