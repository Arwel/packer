import re
from multiprocessing.pool import Pool

from shutil import rmtree
from os import remove


class Delete:
    _black_regex = '.*(node_mod|webpack|gulp|\.lock|composer|package\.json|postcss|lock\.json|\.sh).*'
    _compiled_regex = None

    def __init__(self):
        if self._compiled_regex is None:
            self._compiled_regex = re.compile(self._black_regex)

    def _walk(self, path):
        if not path.exists():
            return True
        elif str(path.name)[0] == '.' or self._compiled_regex.search(str(path)):
            self._remove(path)
        elif path.is_dir():
            for item in path.iterdir():
                self._walk(item)

    @staticmethod
    def _remove(path):
        if path.is_dir():
            return rmtree(path)
        else:
            return remove(path)

    def run(self, path_list):
        pool = Pool(10)
        result = pool.map_async(self._walk, path_list)
        result.wait()
